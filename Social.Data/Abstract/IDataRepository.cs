﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Social.Domain.Entities;

namespace Social.Data.Abstract
{
    public interface IDataRepository : IUser
    {
        IEnumerable<UserProfile> UserProfiles { get; }
        UserProfile UserProfilesDoAdd(UserProfile userProfile);
        UserProfile UserProfilesDoRemove(UserProfile userProfile);


        IEnumerable<FriendDependence> FriendDependencies { get; }
        FriendDependence FriendDependenciesDoAdd(FriendDependence friendDependencies);
        FriendDependence FriendDependenciesDoRemove(FriendDependence friendDependencies);
        Task<UserProfile> FindUserProfileByUserProfileIdAsync(int userProfileId);
        Task<List<FriendDependence>> FindFriendDependenceListByUserProfileIdAsync(int userProfileId);
        Task<FriendDependence> FindFriendDependenceBySelfAndFriendUserProfileIdAsync(int selfUserProfileId,
            int friendUserProfileId);
        Task<long> GetFriendDependenciesLongCountAsync(int selfUserProfileId, int friendUserProfileId);

        int SaveChanges();
    }
}
