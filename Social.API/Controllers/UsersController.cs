﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using AutoMapper;
using Social.Data.Abstract;
using Social.API.Models;
using Social.API.Helpers;
using Social.Domain.Entities;
using System.Collections.Specialized;
using System.Security.Claims;
using Newtonsoft.Json;
using System.Web.Configuration;

namespace Social.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/users")]
    public class UsersController : ApiController
    {
        private readonly IUser _repository;
        private readonly IFileService _fileService;
        public UsersController(IDataRepository repository, IFileService  fileService)
        {
            this._repository  = repository;
            this._fileService = fileService;
        }
        [HttpGet]
        [Route("{name}", Name = "GetUserAsync")]
        public async Task<IHttpActionResult> GetUserAsync(string name)
        {

            var userFromRepo = await _repository.GetUserAsync(name);

            if (userFromRepo == null)
            {
                return NotFound();
            }

            var user = Mapper.Map<UserProfileDto>(userFromRepo);
            return Ok(user);
        }


        [HttpGet]
        [Route(Name = "GetUsersAsync")]
        public async Task<IHttpActionResult> GetUsersAsync([FromUri] UsersResourceParameters usersResourceParameters)
        {
            var usersFromRepo = await _repository.GetUsersAsync(usersResourceParameters);

            var previousPageLink = usersFromRepo.HasPrevious ?
                    CreateUsersResourceUri(usersResourceParameters,
                    ResourceUriType.PreviousPage) : null;

            var nextPageLink = usersFromRepo.HasNext ?
                CreateUsersResourceUri(usersResourceParameters,
                ResourceUriType.NextPage) : null;

            var paginationMetadata = new
            {
                totalCount = usersFromRepo.TotalCount,
                pageSize = usersFromRepo.PageSize,
                currentPage = usersFromRepo.CurrentPage,
                totalPages = usersFromRepo.TotalPages,
                previousPageLink = previousPageLink,
                nextPageLink = nextPageLink
            };

            var users = Mapper.Map<IEnumerable<UserProfileDto>>(usersFromRepo);

            var response = Request.CreateResponse(HttpStatusCode.OK, users);

            response.Headers.Add("X-Pagination",
                Newtonsoft.Json.JsonConvert.SerializeObject(paginationMetadata));

            return ResponseMessage(response);
        }

        private string CreateUsersResourceUri(
                    UsersResourceParameters usersResourceParameters,
                    ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return Url.Route("GetUsersAsync",
                      new
                      {
                          searchQuery = usersResourceParameters.SearchQuery,
                          pageNumber = usersResourceParameters.PageNumber - 1,
                          pageSize = usersResourceParameters.PageSize
                      });
                case ResourceUriType.NextPage:
                    return Url.Route("GetUsersAsync",
                      new
                      {
                          searchQuery = usersResourceParameters.SearchQuery,
                          pageNumber = usersResourceParameters.PageNumber + 1,
                          pageSize = usersResourceParameters.PageSize
                      });

                default:
                    return Url.Route("GetUsersAsync",
                    new
                    {
                        searchQuery = usersResourceParameters.SearchQuery,
                        pageNumber = usersResourceParameters.PageNumber,
                        pageSize = usersResourceParameters.PageSize
                    });
            }
        }

        [HttpPut]
        [Route(Name = "UpsertUserAsync")]
        public async Task<IHttpActionResult> UpsertUserAsync()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            var provider = await Request.Content.ReadAsMultipartAsync<InMemoryMultipartFormDataStreamProvider>(new InMemoryMultipartFormDataStreamProvider());

            NameValueCollection formData = provider.FormData;

            var user = JsonConvert.DeserializeObject<UserProfileDto>(formData["user"]);

            var identity = User.Identity as ClaimsIdentity;
            Claim identityClaim = identity?.Claims.FirstOrDefault(c => c.Type == "sub");
            if (user.UserName.ToLower() == identityClaim?.Value.ToLower())
            {
                user.UserName = identityClaim.Value;
            }
            else
            {
                return BadRequest("You are logged in under a different user name.");
            }

            IList<HttpContent> files = provider.Files;

            var fileUrl = "";

            if(files.Count != 0)
            {
                HttpContent imageFile = files[0];
                fileUrl = await _fileService.SaveImageFile(imageFile);
            }
            else
            {
                fileUrl = user.ImagePath;
            }
            
                         
            if (!ModelState.IsValid)
            {
                return StatusCode((HttpStatusCode)422);
            }


            var userFromRepo = await _repository.GetUserAsync(user.UserName);
            if (userFromRepo == null)
            {
                var userToAdd = Mapper.Map<UserProfile>(user);
                userToAdd.LastEditedAt = DateTime.Now;

                string userImagePath = fileUrl;
                if (!string.IsNullOrEmpty(userImagePath))
                {
                    userToAdd.ImagePath = userImagePath;
                }

                _repository.AddUser(userToAdd);

                if (!await _repository.SaveAsync())
                {
                    throw new Exception($"Upserting user {user.UserName} failed on save.");
                }

                var userToReturn = Mapper.Map<UserProfileDto>(userToAdd);

                return CreatedAtRoute("GetUserAsync",
                    new { name = user.UserName },
                    userToReturn);
            }

            user.ImagePath = fileUrl;

            Mapper.Map(user, userFromRepo);

            _repository.UpdateUser(userFromRepo);

            if (!await _repository.SaveAsync())
            {
                throw new Exception($"Updating user {user.UserName} failed on save.");
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

    }
}