﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Social.Domain.Entities
{
    public class UserProfile
    {
        public int UserProfileId { get; set; }

        [DisplayName("User name")]
        [Required]
        [DataType(DataType.Text)]
        [RegularExpression(@"^[a-zA-Z0-9- ]+$", ErrorMessage = "Use only english characters, digits or hyphen.")]
        [StringLength(255, MinimumLength = 1, ErrorMessage = "Length must be between 1 and 255 characters.")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please specify contact email")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Mailing address is not valid")]
        [Display(Name = "Your contact email")]
        public string ContactEmail { get; set; }

        [DisplayName("Photo")]
        [DataType(DataType.Text)]
        [StringLength(1024)]
        public string ImagePath { get; set; }

        [DisplayName("Last edited by")]
        [DataType(DataType.Text)]
        public string LastEditedBy { get; set; }

        [DisplayName("Last edited at (UTC)")]
        [DataType(DataType.DateTime)]
        public DateTime LastEditedAt { get; set; }
    }
}
