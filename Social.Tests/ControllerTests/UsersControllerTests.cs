﻿using NUnit.Framework;
using Social.API.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Social.Data.Abstract;
using Social.API.Helpers;
using System.Security.Claims;
using System.Threading;
using Social.Domain.Entities;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;
using System.Web.Http.Results;
using System.Web.Http.Routing;
using System.Web.UI.WebControls;
using AutoMapper;
using FluentAssertions;
using Newtonsoft.Json;
using Social.API.Models;

//using AutoMoq;
namespace Social.Tests.ControllerTests
{
    [TestFixture]
    public class UsersControllerTests
    {
        private UsersController userController;
        private Mock<IDataRepository> dataRepository;
        private Mock<IFileService> fileService;

        [SetUp]
        public void SetUp()
        {
            dataRepository = new Mock<IDataRepository>();
            fileService = new Mock<IFileService>();

            var identity = new GenericIdentity("TestUser","sub");
            identity.AddClaim(new Claim("sub", "TestUser"));

            fileService
                .Setup(i => i.SaveImageFile(It.IsAny<HttpContent>()))
                .ReturnsAsync("image.com");

            var principal = new GenericPrincipal(identity, roles: new string[] { });
            var user = new ClaimsPrincipal(principal);

            userController = new UsersController(dataRepository.Object, fileService.Object)
            {
                User = user
            };

            Mapper.Initialize((config) =>
            {
                config.CreateMap<UserProfile, UserProfileDto>();
                config.CreateMap<UserProfileDto, UserProfile>()
                    .BeforeMap((s, d) => d.LastEditedBy = "Server")
                    .BeforeMap((s, d) => d.LastEditedAt = DateTime.Now);
            });

        }

        [Test]
        public async Task TestGetUserAsyncShouldReturnUser()
        {
            var userFromRepo = new UserProfile()
            {
                ContactEmail = "test@mail.cin",
                ImagePath = "testImage",
                LastEditedAt = DateTime.MaxValue,
                LastEditedBy = "Admin",
                UserName = "TestUser",
                UserProfileId = 2
            };

            dataRepository
                .Setup(p => p.GetUserAsync("TestUser"))
                .ReturnsAsync(userFromRepo);

            var result = (await userController.GetUserAsync("TestUser") as OkNegotiatedContentResult<UserProfileDto>)
                .Content;

            Assert.NotNull(result);
            result.ShouldBeEquivalentTo(Mapper.Map<UserProfileDto>(userFromRepo));
            dataRepository.Verify(x => x.GetUserAsync("TestUser"), Times.Once);
        }

        [Test]
        public async Task TestGetUsersAsyncShouldReturnUsers()
        {
            userController.Request = new HttpRequestMessage();
            userController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey,
                new HttpConfiguration());
            var parameters = new UsersResourceParameters();

            var userList = GetUsers();

            var users = PagedList<UserProfile>.Create(userList, parameters.PageNumber, parameters.PageSize);

            dataRepository
                .Setup(x => x.GetUsersAsync(parameters))
                .ReturnsAsync(users);

            var response =
                await userController.GetUsersAsync(parameters)
                    as ResponseMessageResult;

            Assert.NotNull(response);

            var content = UserTestHelper.ConvertResponseMessageResultToResult<IEnumerable<UserProfileDto>>(response);

            Assert.AreEqual(GetUsers().Count(), content.Count());

            Assert.IsTrue(response.Response.Headers.Contains("X-Pagination"));

            content.ShouldBeEquivalentTo(Mapper.Map<IEnumerable<UserProfileDto>>(GetUsers()));

            dataRepository.Verify(x => x.GetUsersAsync(parameters), Times.Once);

        }

        [Test]
        public async Task TestUpsertUserAsyncShouldAddUser()
        {
            dataRepository
                .Setup(u => u.GetUserAsync("TestUser"))
                .ReturnsAsync((UserProfile) null);

            dataRepository
                .Setup(u => u.SaveAsync())
                .ReturnsAsync(true);

            var userToAdd = new UserProfileDto()
            {
                UserName = "TestUser",
                ContactEmail = "sdfdsf@mail.ri",
                ImagePath = "test.image"
            };

            userController.ControllerContext = UserTestHelper.CreateMultipartFormDataContentForHttpContext(userToAdd, userController.ControllerContext);
            var response =
                await userController.UpsertUserAsync() as CreatedAtRouteNegotiatedContentResult<UserProfileDto>;

            Assert.NotNull(response);

            var userFromResponse = response.Content ;

            dataRepository.Verify(a => a.AddUser(It.Is<UserProfile>(i =>
                    i.UserName     == userToAdd.UserName
                 && i.ContactEmail == userToAdd.ContactEmail
                 && i.ImagePath    == "image.com"
            )), Times.Once);
        }

        [Test]
        public async Task TestUpsertUserAsyncShouldUpdateUser()
        {
            var userFromRepo = new UserProfile()
            {
                ContactEmail = "test@mail.cin",
                ImagePath = "testImage",
                LastEditedAt = DateTime.MaxValue,
                LastEditedBy = "Admin",
                UserName = "TestUser",
                UserProfileId = 2
            };

            var userForUpdate = new UserProfileDto()
            {
                UserName = "UpdatedUser",
                ContactEmail = "updated.com",
                ImagePath = "image.com"
            };

            dataRepository
                .Setup(u => u.GetUserAsync("TestUser"))
                .ReturnsAsync(userFromRepo);

            dataRepository
                .Setup(u => u.SaveAsync())
                .ReturnsAsync(true);

            userController.ControllerContext = UserTestHelper.CreateMultipartFormDataContentForHttpContext(userForUpdate, userController.ControllerContext);

            var response = await userController.UpsertUserAsync();

            Assert.NotNull(response);

            dataRepository.Verify(a => a.UpdateUser(It.Is<UserProfile>(i =>
                i.UserName == userForUpdate.UserName
                && i.ContactEmail == userForUpdate.ContactEmail
                && i.ImagePath == "image.com"
            )), Times.Once);
        }

        private IEnumerable<UserProfile> GetUsers()
        {
            return new List<UserProfile>
            {
                new UserProfile()
                {
                    ContactEmail = "test@mail.cin",
                    ImagePath = "testImage",
                    LastEditedAt = DateTime.MaxValue,
                    LastEditedBy = "Admin",
                    UserName = "TestUser1",
                    UserProfileId = 1
                },
                new UserProfile()
                {
                    ContactEmail = "test@mail.cin",
                    ImagePath = "testImage",
                    LastEditedAt = DateTime.MaxValue,
                    LastEditedBy = "Admin",
                    UserName = "TestUser2",
                    UserProfileId = 2
                },
                new UserProfile()
                {
                    ContactEmail = "test@mail.cin",
                    ImagePath = "testImage",
                    LastEditedAt = DateTime.MaxValue,
                    LastEditedBy = "Admin3",
                    UserName = "TestUser",
                    UserProfileId = 3
                }
            };
        }
    }
}
