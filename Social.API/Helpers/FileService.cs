﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using Microsoft.Azure; // Namespace for Azure Configuration Manager
using Microsoft.WindowsAzure.Storage; // Namespace for Storage Client Library
using Microsoft.WindowsAzure.Storage.Blob; // Namespace for Azure Blobs
using Microsoft.WindowsAzure.Storage.File;

namespace Social.API.Helpers
{
    public  class FileService : IFileService
    {
        private CloudStorageAccount _storageAccount;
        private CloudBlobContainer _container;
        public FileService()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            CloudConfigurationManager.GetSetting("StorageConnectionString"));
            this._storageAccount = storageAccount;
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");
            container.CreateIfNotExists();
            this._container = container;
        }

        public  async Task<string> SaveImageFile(HttpContent file)
        {

            string fileName = file.Headers.ContentDisposition.FileName.Trim('\"');
            var blob = _container.GetBlockBlobReference(fileName);
            using (Stream input = await file.ReadAsStreamAsync())
            {
                await blob.UploadFromStreamAsync(input);
            }
            
            return blob.Uri.AbsoluteUri;

        }
    }
}