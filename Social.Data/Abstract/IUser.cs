﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Social.Domain.Entities;
using Social.API.Helpers;

namespace Social.Data.Abstract
{
    public interface IUser
    {
        Task<UserProfile> GetUserAsync(string user);

        Task<IEnumerable<UserProfile>> GetUsersAsync();

        Task<PagedList<UserProfile>> GetUsersAsync(UsersResourceParameters usersResourceParameters);

        Task<bool> UserExistsAsync(string name);

        Task<bool> SaveAsync();

        void AddUser(UserProfile user);

        void UpdateUser(UserProfile user);
    }
}
