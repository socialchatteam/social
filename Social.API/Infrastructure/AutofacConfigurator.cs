﻿using System;
using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Social.Data.Abstract;
using Social.Data.Concrete;
using Social.API.Helpers;

namespace Social.API.Infrastructure
{
    public static class AutofacConfigurator
    {
        public static IContainer Configure(Assembly assembly, HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(assembly);

            // Register types
            builder.RegisterType<AuthRepository>().As<IAuthRepository>().InstancePerRequest();
            builder.RegisterType<DataRepository>().As<IDataRepository>().InstancePerRequest();
            builder.RegisterType<FileService>().As<IFileService>().SingleInstance();
            var container = builder.Build();

            //CreateSingletons(container);

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            return container;
        }

        /// <summary>
        /// Creates all singletons on startup
        /// </summary>
        private static void CreateSingletons(IContainer container)
        {
            throw  new NotImplementedException();
        }
    }
}
