﻿using System;
using AutoMapper;
using Social.API.Models;
using Social.Domain.Entities;

namespace Social.API
{
    public static class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize((config) =>
            {
                config.CreateMap<UserProfile, UserProfileDto>();
                config.CreateMap<UserProfile, FriendProfileDto>();
                config.CreateMap<UserProfileDto, UserProfile>()
                    .BeforeMap((s, d) => d.LastEditedBy = "Server")
                    .BeforeMap((s, d) => d.LastEditedAt = DateTime.Now);
            });
        }
    }
}