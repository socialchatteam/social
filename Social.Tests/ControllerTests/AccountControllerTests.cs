﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Http.Results;
using Microsoft.AspNet.Identity;
using Moq;
using NUnit.Framework;
using Social.API.Controllers;
using Social.Data.Abstract;
using Social.Domain.Entities;

namespace Social.Tests.ControllerTests
{
    public class AccountControllerTests
    {
        private AccountController accountController;
        private Mock<IAuthRepository> authRepository;
        private Mock<IDataRepository> dataRepository;


        [SetUp]
        public void SetUp()
        {
            authRepository = new Mock<IAuthRepository>();
            dataRepository = new Mock<IDataRepository>();
            accountController = new AccountController(authRepository.Object, dataRepository.Object);
        }

        [Test]
        public async Task Can_Register_User()
        {
            // Arrange
            UserAuthModel userModel = new UserAuthModel
            {
                UserName = "TestUser",
                Password = "123456aA!",
                ConfirmPassword = "123456aA!"
            };

            var responseTask = Task.FromResult(IdentityResult.Success);
            authRepository.Setup(x => x.RegisterUser(userModel)).Returns(responseTask);

            // Action
            var result = await accountController.Register(userModel);

            // Assert
            Assert.IsInstanceOf<OkResult>(result);
        }
    }
}
