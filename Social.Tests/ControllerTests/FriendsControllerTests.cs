﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web.Http.Results;
using AutoMapper;
using Moq;
using NUnit.Framework;
using Social.API.Controllers;
using Social.API.Models;
using Social.Data.Abstract;
using Social.Domain.Entities;

namespace Social.Tests.ControllerTests
{
    public class FriendsControllerTests
    {
        private FriendsController friendsController;
        private Mock<IDataRepository> dataRepository;


        [SetUp]
        public void SetUp()
        {
            Mapper.Initialize(config =>
            {
                config.CreateMap<UserProfile, UserProfileDto>();
                config.CreateMap<UserProfile, FriendProfileDto>();
                config.CreateMap<UserProfileDto, UserProfile>();
            });

            dataRepository = new Mock<IDataRepository>();

            var identity = new GenericIdentity("TestUser", "sub");
            identity.AddClaim(new Claim("sub", "TestUser"));
            var principal = new GenericPrincipal(identity, roles: new string[] { });
            var user = new ClaimsPrincipal(principal);

            friendsController = new FriendsController(dataRepository.Object)
            {
                User = user
            };
        }

        [Test]
        public async Task Can_Get_All_Friends()
        {
            // Arrange
            string selfname = "TestUser";
            string friendname = "";

            UserProfile userProfile = new UserProfile
            {
                UserProfileId = 1,
                UserName = selfname,
                ContactEmail = "no-reply@example.com",
                LastEditedAt = DateTime.Now
            };

            dataRepository.Setup(x => x.GetUserAsync(selfname)).ReturnsAsync(userProfile);

            List<FriendDependence> fdList = new List<FriendDependence>
            {
                new FriendDependence { FriendDependenceId = 1, SelfUserProfileId = 1, FriendUserProfileId = 2},
                new FriendDependence { FriendDependenceId = 2, SelfUserProfileId = 3, FriendUserProfileId = 6},
                new FriendDependence { FriendDependenceId = 3, SelfUserProfileId = 4, FriendUserProfileId = 5}
            };

            dataRepository.Setup(x => x.FindFriendDependenceListByUserProfileIdAsync(1)).Returns(Task.FromResult(fdList));

            UserProfile friendProfile = new UserProfile
            {
                UserProfileId = 2,
                UserName = "FriendName",
                ContactEmail = "no-reply@example.com",
                LastEditedAt = DateTime.Now
            };

            dataRepository.Setup(x => x.FindUserProfileByUserProfileIdAsync(2)).ReturnsAsync(friendProfile);

            // Action
            var result = await friendsController.GetFriendsByFilterAsync(selfname, friendname) as OkNegotiatedContentResult<List<FriendProfileDto>>;

            // Assert
            int? countFriends = result?.Content.Count;
            Assert.AreEqual(1 , countFriends);
        }

        [Test]
        public async Task Can_Get_Friend_By_Filter()
        {
            // Arrange
            string selfname = "TestUser";
            string friendname = "FriendName";

            UserProfile userProfile = new UserProfile
            {
                UserProfileId = 1,
                UserName = selfname,
                ContactEmail = "no-reply@example.com",
                LastEditedAt = DateTime.Now
            };

            dataRepository.Setup(x => x.GetUserAsync(selfname)).ReturnsAsync(userProfile);

            List<FriendDependence> fdList = new List<FriendDependence>
            {
                new FriendDependence { FriendDependenceId = 1, SelfUserProfileId = 1, FriendUserProfileId = 2},
                new FriendDependence { FriendDependenceId = 2, SelfUserProfileId = 3, FriendUserProfileId = 6},
                new FriendDependence { FriendDependenceId = 3, SelfUserProfileId = 4, FriendUserProfileId = 5}
            };

            dataRepository.Setup(x => x.FindFriendDependenceListByUserProfileIdAsync(1)).ReturnsAsync(fdList);

            UserProfile friendProfile = new UserProfile
            {
                UserProfileId = 2,
                UserName = friendname,
                ContactEmail = "no-reply@example.com",
                LastEditedAt = DateTime.Now
            };

            dataRepository.Setup(x => x.FindUserProfileByUserProfileIdAsync(2)).ReturnsAsync(friendProfile);

            // Action
            var result = await friendsController.GetFriendsByFilterAsync(selfname, friendname) as OkNegotiatedContentResult<List<FriendProfileDto>>;

            // Assert
            int? countFriends = result?.Content.Count;
            Assert.AreEqual(1, countFriends);
        }

        [Test]
        public async Task Can_Add_Friend()
        {
            // Arrange
            string selfname = "TestUser";
            string friendname = "FriendName";

            UserProfile userProfile = new UserProfile
            {
                UserProfileId = 1,
                UserName = selfname,
                ContactEmail = "no-reply@example.com",
                LastEditedAt = DateTime.Now
            };

            UserProfile friendProfile = new UserProfile
            {
                UserProfileId = 2,
                UserName = friendname,
                ContactEmail = "no-reply@example.com",
                LastEditedAt = DateTime.Now
            };

            dataRepository.Setup(x => x.GetUserAsync(selfname)).Returns(Task.FromResult(userProfile));
            dataRepository.Setup(x => x.GetUserAsync(friendname)).Returns(Task.FromResult(friendProfile));

            dataRepository
                .Setup(x => x.GetFriendDependenciesLongCountAsync(userProfile.UserProfileId,
                    friendProfile.UserProfileId)).ReturnsAsync(0L);

            dataRepository
                .Setup(x => x.GetFriendDependenciesLongCountAsync(friendProfile.UserProfileId,
                    userProfile.UserProfileId)).ReturnsAsync(0L);

            FriendDependence friendDependence = new FriendDependence
            {
                SelfUserProfileId = userProfile.UserProfileId,
                FriendUserProfileId = friendProfile.UserProfileId
            };

            dataRepository.Setup(x => x.FriendDependenciesDoAdd(friendDependence)).Returns(friendDependence);

            FriendDependence friendDependenceBackward = new FriendDependence
            {
                SelfUserProfileId = friendProfile.UserProfileId,
                FriendUserProfileId = userProfile.UserProfileId
            };

            dataRepository.Setup(x => x.FriendDependenciesDoAdd(friendDependenceBackward)).Returns(friendDependenceBackward);

            dataRepository.Setup(x => x.SaveChanges()).Returns(1);

            // Action
            var result = await friendsController.AddFriend(selfname, friendname);

            // Assert
            Assert.IsInstanceOf<OkResult>(result);
        }

        [Test]
        public async Task Cannot_Add_ExistFriend()
        {
            // Arrange
            string selfname = "TestUser";
            string friendname = "FriendName";

            UserProfile userProfile = new UserProfile
            {
                UserProfileId = 1,
                UserName = selfname,
                ContactEmail = "no-reply@example.com",
                LastEditedAt = DateTime.Now
            };

            UserProfile friendProfile = new UserProfile
            {
                UserProfileId = 2,
                UserName = friendname,
                ContactEmail = "no-reply@example.com",
                LastEditedAt = DateTime.Now
            };

            dataRepository.Setup(x => x.GetUserAsync(selfname)).Returns(Task.FromResult(userProfile));
            dataRepository.Setup(x => x.GetUserAsync(friendname)).Returns(Task.FromResult(friendProfile));

            dataRepository
                .Setup(x => x.GetFriendDependenciesLongCountAsync(userProfile.UserProfileId,
                    friendProfile.UserProfileId)).ReturnsAsync(1L);

            dataRepository
                .Setup(x => x.GetFriendDependenciesLongCountAsync(friendProfile.UserProfileId,
                    userProfile.UserProfileId)).ReturnsAsync(1L);

            FriendDependence friendDependence = new FriendDependence
            {
                SelfUserProfileId = userProfile.UserProfileId,
                FriendUserProfileId = friendProfile.UserProfileId
            };

            dataRepository.Setup(x => x.FriendDependenciesDoAdd(friendDependence)).Returns(friendDependence);

            FriendDependence friendDependenceBackward = new FriendDependence
            {
                SelfUserProfileId = friendProfile.UserProfileId,
                FriendUserProfileId = userProfile.UserProfileId
            };

            dataRepository.Setup(x => x.FriendDependenciesDoAdd(friendDependenceBackward)).Returns(friendDependenceBackward);

            dataRepository.Setup(x => x.SaveChanges()).Returns(1);

            // Action
            var result = await friendsController.AddFriend(selfname, friendname);

            // Assert
            Assert.IsInstanceOf<BadRequestResult>(result);
        }

        [Test]
        public async Task Cannot_Add_Friend_From_Fraud_SelfName()
        {
            // Arrange
            string selfname = "FraudUser";
            string friendname = "FriendName";

            UserProfile userProfile = new UserProfile
            {
                UserProfileId = 1,
                UserName = selfname,
                ContactEmail = "no-reply@example.com",
                LastEditedAt = DateTime.Now
            };

            UserProfile friendProfile = new UserProfile
            {
                UserProfileId = 2,
                UserName = friendname,
                ContactEmail = "no-reply@example.com",
                LastEditedAt = DateTime.Now
            };

            dataRepository.Setup(x => x.GetUserAsync(selfname)).Returns(Task.FromResult(userProfile));
            dataRepository.Setup(x => x.GetUserAsync(friendname)).Returns(Task.FromResult(friendProfile));

            dataRepository
                .Setup(x => x.GetFriendDependenciesLongCountAsync(userProfile.UserProfileId,
                    friendProfile.UserProfileId)).ReturnsAsync(1L);

            dataRepository
                .Setup(x => x.GetFriendDependenciesLongCountAsync(friendProfile.UserProfileId,
                    userProfile.UserProfileId)).ReturnsAsync(1L);

            FriendDependence friendDependence = new FriendDependence
            {
                SelfUserProfileId = userProfile.UserProfileId,
                FriendUserProfileId = friendProfile.UserProfileId
            };

            dataRepository.Setup(x => x.FriendDependenciesDoAdd(friendDependence)).Returns(friendDependence);

            FriendDependence friendDependenceBackward = new FriendDependence
            {
                SelfUserProfileId = friendProfile.UserProfileId,
                FriendUserProfileId = userProfile.UserProfileId
            };

            dataRepository.Setup(x => x.FriendDependenciesDoAdd(friendDependenceBackward)).Returns(friendDependenceBackward);

            dataRepository.Setup(x => x.SaveChanges()).Returns(1);

            // Action
            var result = await friendsController.AddFriend(selfname, friendname);

            // Assert
            Assert.IsInstanceOf<BadRequestErrorMessageResult>(result);
        }

        [Test]
        public async Task Can_Remove_Friend()
        {
            // Arrange
            string selfname = "TestUser";
            string friendname = "FriendName";

            UserProfile userProfile = new UserProfile
            {
                UserProfileId = 1,
                UserName = selfname,
                ContactEmail = "no-reply@example.com",
                LastEditedAt = DateTime.Now
            };

            UserProfile friendProfile = new UserProfile
            {
                UserProfileId = 2,
                UserName = friendname,
                ContactEmail = "no-reply@example.com",
                LastEditedAt = DateTime.Now
            };

            dataRepository.Setup(x => x.GetUserAsync(selfname)).ReturnsAsync(userProfile);
            dataRepository.Setup(x => x.GetUserAsync(friendname)).ReturnsAsync(friendProfile);

            FriendDependence friendDependence = new FriendDependence
            {
                SelfUserProfileId = userProfile.UserProfileId,
                FriendUserProfileId = friendProfile.UserProfileId
            };

            FriendDependence friendDependenceBackward = new FriendDependence
            {
                SelfUserProfileId = friendProfile.UserProfileId,
                FriendUserProfileId = userProfile.UserProfileId
            };

            dataRepository.Setup(x => x.FindFriendDependenceBySelfAndFriendUserProfileIdAsync(userProfile.UserProfileId, friendProfile.UserProfileId)).ReturnsAsync(friendDependence);
            dataRepository.Setup(x => x.FindFriendDependenceBySelfAndFriendUserProfileIdAsync(friendProfile.UserProfileId, userProfile.UserProfileId)).ReturnsAsync(friendDependenceBackward);

            dataRepository.Setup(x => x.FriendDependenciesDoRemove(friendDependence)).Returns(friendDependence);
            dataRepository.Setup(x => x.FriendDependenciesDoRemove(friendDependenceBackward)).Returns(friendDependenceBackward);

            dataRepository.Setup(x => x.SaveChanges()).Returns(1);

            // Action
            var result = await friendsController.RemoveFriend(selfname, friendname);

            // Assert
            Assert.IsInstanceOf<OkResult>(result);
        }

        [Test]
        public async Task Cannot_Remove_Friend_From_Fraud_SelfName()
        {
            // Arrange
            string selfname = "FraudUser";
            string friendname = "FriendName";

            UserProfile userProfile = new UserProfile
            {
                UserProfileId = 1,
                UserName = selfname,
                ContactEmail = "no-reply@example.com",
                LastEditedAt = DateTime.Now
            };

            UserProfile friendProfile = new UserProfile
            {
                UserProfileId = 2,
                UserName = friendname,
                ContactEmail = "no-reply@example.com",
                LastEditedAt = DateTime.Now
            };

            dataRepository.Setup(x => x.GetUserAsync(selfname)).ReturnsAsync(userProfile);
            dataRepository.Setup(x => x.GetUserAsync(friendname)).ReturnsAsync(friendProfile);

            FriendDependence friendDependence = new FriendDependence
            {
                SelfUserProfileId = userProfile.UserProfileId,
                FriendUserProfileId = friendProfile.UserProfileId
            };

            FriendDependence friendDependenceBackward = new FriendDependence
            {
                SelfUserProfileId = friendProfile.UserProfileId,
                FriendUserProfileId = userProfile.UserProfileId
            };

            dataRepository.Setup(x => x.FindFriendDependenceBySelfAndFriendUserProfileIdAsync(userProfile.UserProfileId, friendProfile.UserProfileId)).ReturnsAsync(friendDependence);
            dataRepository.Setup(x => x.FindFriendDependenceBySelfAndFriendUserProfileIdAsync(friendProfile.UserProfileId, userProfile.UserProfileId)).ReturnsAsync(friendDependenceBackward);

            dataRepository.Setup(x => x.FriendDependenciesDoRemove(friendDependence)).Returns(friendDependence);
            dataRepository.Setup(x => x.FriendDependenciesDoRemove(friendDependenceBackward)).Returns(friendDependenceBackward);

            dataRepository.Setup(x => x.SaveChanges()).Returns(1);

            // Action
            var result = await friendsController.RemoveFriend(selfname, friendname);

            // Assert
            Assert.IsInstanceOf<BadRequestErrorMessageResult>(result);
        }
    }
    
}
