namespace Social.Data.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FriendDependences",
                c => new
                    {
                        FriendDependenceId = c.Int(nullable: false, identity: true),
                        SelfUserProfileId = c.Int(nullable: false),
                        FriendUserProfileId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FriendDependenceId);
            
            CreateTable(
                "dbo.UserProfiles",
                c => new
                    {
                        UserProfileId = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false, maxLength: 255),
                        ContactEmail = c.String(nullable: false),
                        ImagePath = c.String(maxLength: 1024),
                        LastEditedBy = c.String(),
                        LastEditedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.UserProfileId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserProfiles");
            DropTable("dbo.FriendDependences");
        }
    }
}
