﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Social.API.Models;
using Social.Data.Abstract;
using Social.Domain.Entities;

namespace Social.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/friends")]
    public class FriendsController : ApiController
    {
        private readonly IDataRepository _repository;
        public FriendsController(IDataRepository repository)
        {
            this._repository = repository;
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetFriendsByFilterAsync([FromUri]string selfame, [FromUri]string friendnamefilter = "")
        {
            var userProfile = await _repository.GetUserAsync(selfame);

            if (userProfile == null)
            {
                return BadRequest();
            }

            List<FriendDependence> friendDependenceList = await _repository.FindFriendDependenceListByUserProfileIdAsync(userProfile.UserProfileId);

            List<FriendProfileDto> friendProfileDtoList = new List<FriendProfileDto>();

            foreach (var friendDependence in friendDependenceList)
            {
                var friend = await _repository.FindUserProfileByUserProfileIdAsync(friendDependence.FriendUserProfileId);

                if (friend != null)
                {
                    if (friend.UserName.StartsWith(friendnamefilter) | string.IsNullOrEmpty(friendnamefilter))
                    {
                        friendProfileDtoList.Add(Mapper.Map<FriendProfileDto>(friend));
                    }
                }
            }

            return Ok(friendProfileDtoList);
        }

        [HttpPost]
        public async Task<IHttpActionResult> AddFriend(string selfame, string friendname)
        {
            var identity = User.Identity as ClaimsIdentity;
            Claim identityClaim = identity?.Claims.FirstOrDefault(c => c.Type == "sub");
            if (selfame.ToLower() == identityClaim?.Value.ToLower())
            {
                selfame = identityClaim.Value;
            }
            else
            {
                return BadRequest("You are logged in under a different user name.");
            }

            if (selfame.ToLower() == friendname.ToLower())
            {
                return BadRequest();
            }

            var userProfile = await _repository.GetUserAsync(selfame);
            var friendProfile = await _repository.GetUserAsync(friendname);

            if (userProfile == null || friendProfile == null)
            {
                return BadRequest();
            }

            long alreadyCounterSelf = await _repository.GetFriendDependenciesLongCountAsync(userProfile.UserProfileId, friendProfile.UserProfileId);
            long alreadyCounterBackward = await _repository.GetFriendDependenciesLongCountAsync(friendProfile.UserProfileId, userProfile.UserProfileId);

            if (alreadyCounterSelf == 0)
            {
                FriendDependence friendDependence = new FriendDependence
                {
                    SelfUserProfileId = userProfile.UserProfileId,
                    FriendUserProfileId = friendProfile.UserProfileId
                };

                _repository.FriendDependenciesDoAdd(friendDependence);

                if (alreadyCounterBackward == 0)
                {
                    FriendDependence friendDependenceBackward = new FriendDependence
                    {
                        SelfUserProfileId = friendProfile.UserProfileId,
                        FriendUserProfileId = userProfile.UserProfileId
                    };

                    _repository.FriendDependenciesDoAdd(friendDependenceBackward);
                }

                if (_repository.SaveChanges() > 0)
                {
                    return Ok();
                }
                return BadRequest();
            }
            return BadRequest();
        }

        [HttpDelete]
        public async Task<IHttpActionResult> RemoveFriend([FromBody]string selfame, [FromBody]string friendname)
        {
            var identity = User.Identity as ClaimsIdentity;
            Claim identityClaim = identity?.Claims.FirstOrDefault(c => c.Type == "sub");
            if (selfame.ToLower() == identityClaim?.Value.ToLower())
            {
                selfame = identityClaim.Value;
            }
            else
            {
                return BadRequest("You are logged in under a different user name.");
            }

            var userProfile = await _repository.GetUserAsync(selfame);
            var friendProfile = await _repository.GetUserAsync(friendname);

            if (userProfile == null || friendProfile == null)
            {
                return NotFound();
            }

            var friendDependence = await _repository.FindFriendDependenceBySelfAndFriendUserProfileIdAsync(userProfile.UserProfileId, friendProfile.UserProfileId);
            var friendDependenceBackward = await _repository.FindFriendDependenceBySelfAndFriendUserProfileIdAsync(friendProfile.UserProfileId, userProfile.UserProfileId);

            if (friendDependence != null)
            {
                _repository.FriendDependenciesDoRemove(friendDependence);
              
                if (friendDependenceBackward != null)
                {
                    _repository.FriendDependenciesDoRemove(friendDependenceBackward);
                }

                _repository.SaveChanges();

                return Ok();
            }

            return NotFound();
        }
    }
}
