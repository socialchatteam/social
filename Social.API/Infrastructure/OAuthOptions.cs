﻿using System;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;

namespace Social.API.Infrastructure
{
    public static class OAuthOptions
    {
        public static OAuthAuthorizationServerOptions GetOAuthOptions()
        {
            return new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new SimpleAuthorizationServerProvider(),
                AccessTokenExpireTimeSpan = TimeSpan.FromHours(8),
                // In production mode set AllowInsecureHttp = false
                AllowInsecureHttp = true
            };
        }
    }
}
