﻿using System.ComponentModel.DataAnnotations;

namespace Social.Domain.Entities
{
    public class FriendDependence
    {
        public int FriendDependenceId { get; set; }

        [Required]
        public int SelfUserProfileId { get; set; }

        [Required]
        public int FriendUserProfileId { get; set; }
    }
}
