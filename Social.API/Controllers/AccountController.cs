﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Social.Data.Abstract;
using Social.Data.Concrete;
using Social.Domain.Entities;

namespace Social.API.Controllers
{
[RoutePrefix("api/Account")]
public class AccountController : ApiController
{
    private readonly IAuthRepository _authRepository;
    private readonly IDataRepository _dataRepository;

    public AccountController(IAuthRepository authRepository, IDataRepository repository)
    {
        _authRepository = authRepository;
        _dataRepository = repository;
    }

    // POST api/Account/Register
    [AllowAnonymous]
    [Route("Register")]
    public async Task<IHttpActionResult> Register(UserAuthModel userModel)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        IdentityResult result = await _authRepository.RegisterUser(userModel);

        IHttpActionResult errorResult = GetErrorResult(result);

        if (errorResult != null)
        {
            return errorResult;
        }

            // user code for sinc creating users in both DB
        _dataRepository.UserProfilesDoAdd(new UserProfile
        {
            UserName = userModel.UserName,
            ContactEmail = "no-reply@example.com", // we need input email on registration form.
            LastEditedAt = DateTime.Now
        });
            _dataRepository.SaveChanges();

            return Ok();
    }

    protected override void Dispose(bool disposing)
    {
        if (disposing)
        {
            _authRepository.Dispose();
        }

        base.Dispose(disposing);
    }

    private IHttpActionResult GetErrorResult(IdentityResult result)
    {
        if (result == null)
        {
            return InternalServerError();
        }

        if (!result.Succeeded)
        {
            if (result.Errors != null)
            {
                foreach (string error in result.Errors)
                {
                    ModelState.AddModelError("", error);
                }
            }

            if (ModelState.IsValid)
            {
                // No ModelState errors are available to send, so just return an empty BadRequest.
                return BadRequest();
            }

            return BadRequest(ModelState);
        }

        return null;
    }
}
}
