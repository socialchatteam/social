﻿using Microsoft.Owin;
using Owin;
using System;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Social.API.Infrastructure;

[assembly: OwinStartup(typeof(Social.API.Startup))]
namespace Social.API
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureHttpAndAuth(app);
            AutoMapperConfig.Initialize();
            app.MapSignalR();
        }

        public void ConfigureHttpAndAuth(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            var container = AutofacConfigurator.Configure(typeof(Startup).Assembly, config);

            WebApiConfig.Register(config);

            // Token Generation
            app.UseOAuthBearerTokens(OAuthOptions.GetOAuthOptions());
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
            
            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);

            app.Use(async (ctx, next) =>
                {
                    await next();
                })
                .UseWebApi(config);
        }
    }
}