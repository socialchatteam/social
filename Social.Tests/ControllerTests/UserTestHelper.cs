﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Social.API.Models;

namespace Social.Tests.ControllerTests
{
    public  static class UserTestHelper
    {
        public static HttpControllerContext CreateMultipartFormDataContentForHttpContext(UserProfileDto userToAdd, HttpControllerContext context)
        {

            var request = new HttpRequestMessage(HttpMethod.Post, "");
            var content = new MultipartFormDataContent();

            var fileContent = new ByteArrayContent(new Byte[100]);
            fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "Foo.txt"
            };

            var jsonUser = JsonConvert.SerializeObject(userToAdd);
            var user = new StringContent(jsonUser, Encoding.UTF8, "application/json");
            content.Add(fileContent);
            content.Add(user, "user");
            request.Content = content;
            context.Request = request;
            return context;
        }

        public static T ConvertResponseMessageResultToResult<T>(ResponseMessageResult response)
        {
            var jsonContent = response.Response.Content.ReadAsStringAsync().Result;

            return JsonConvert.DeserializeObject<T>(jsonContent);
        }
    }
}
