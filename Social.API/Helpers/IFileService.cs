﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Social.API.Helpers
{
    public interface IFileService
    {
        Task<string> SaveImageFile(HttpContent file);
    }
}