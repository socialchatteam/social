﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Social.API.Models
{
    public class FriendProfileDto
    {
        public string UserName { get; set; }

        public string ContactEmail { get; set; }

        public string ImagePath { get; set; }
    }
}