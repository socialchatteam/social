﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Social.Data.Abstract;
using Social.Domain.Entities;
using Social.Data.Repository;
using Social.API.Helpers;
using System.Linq;

namespace Social.Data.Concrete
{
    public class DataRepository : IDataRepository
    {
        readonly DataContext _dataContext = new DataContext();

        public IEnumerable<UserProfile> UserProfiles
        {
            get { return _dataContext.UserProfiles; } 
            
        }
        public UserProfile UserProfilesDoAdd(UserProfile userProfile)
            => _dataContext.UserProfiles.Add(userProfile);

        public UserProfile UserProfilesDoRemove(UserProfile userProfile)
            => _dataContext.UserProfiles.Remove(userProfile);

        public IEnumerable<FriendDependence> FriendDependencies
        {
            get { return _dataContext.FriendDependencies; } 
            
        }


        public FriendDependence FriendDependenciesDoAdd(FriendDependence friendDependencies)
            => _dataContext.FriendDependencies.Add(friendDependencies);

        public FriendDependence FriendDependenciesDoRemove(FriendDependence friendDependencies)
            => _dataContext.FriendDependencies.Remove(friendDependencies);

        public async Task<UserProfile> FindUserProfileByUserProfileIdAsync(int userProfileId)
            => await _dataContext.UserProfiles.FirstOrDefaultAsync(x => x.UserProfileId == userProfileId);


        public async Task<List<FriendDependence>> FindFriendDependenceListByUserProfileIdAsync(int userProfileId)
            => await _dataContext.FriendDependencies.Where(x => x.SelfUserProfileId == userProfileId).ToListAsync();

        public async Task<FriendDependence> FindFriendDependenceBySelfAndFriendUserProfileIdAsync(int selfUserProfileId, int friendUserProfileId)
            => await _dataContext.FriendDependencies.FirstOrDefaultAsync(x => x.SelfUserProfileId == selfUserProfileId && x.FriendUserProfileId == friendUserProfileId);

        public async Task<long> GetFriendDependenciesLongCountAsync(int selfUserProfileId, int friendUserProfileId)
            => await _dataContext.FriendDependencies.LongCountAsync(
                x => x.SelfUserProfileId == selfUserProfileId && x.FriendUserProfileId == friendUserProfileId);

        public int SaveChanges()
        {
            return _dataContext.SaveChanges();
        }

        public async Task<UserProfile> GetUserAsync(string userName)  
            =>  await _dataContext.UserProfiles.FirstOrDefaultAsync(u => u.UserName == userName);

        public async Task<bool> UserExistsAsync(string name)
            =>  await _dataContext.UserProfiles.AnyAsync(u => u.UserName == name);
    
        public async Task<bool> SaveAsync()
            =>(await _dataContext.SaveChangesAsync() >= 0);

        public void AddUser(UserProfile user)
            =>  _dataContext.UserProfiles.Add(user);

        public void UpdateUser(UserProfile user) 
            => _dataContext.Entry(user).State = EntityState.Modified;

        public async Task<IEnumerable<UserProfile>> GetUsersAsync()
            => await _dataContext.UserProfiles.ToListAsync();

        public async Task<PagedList<UserProfile>> GetUsersAsync(UsersResourceParameters usersResourceParameters)
        {
            var collectionBeforePaging = await GetUsersAsync();

            if (!string.IsNullOrEmpty(usersResourceParameters.SearchQuery))
            {
                var searchQueryForWhereClause = usersResourceParameters.SearchQuery
                    .Trim().ToLowerInvariant();

                collectionBeforePaging = collectionBeforePaging
                    .Where(u => u.UserName.ToLowerInvariant().Contains(searchQueryForWhereClause));

            };

            return PagedList<UserProfile>.Create(collectionBeforePaging,
                            usersResourceParameters.PageNumber,
                            usersResourceParameters.PageSize);
        }
    }
}
