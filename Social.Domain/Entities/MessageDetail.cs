﻿namespace Social.Domain.Entities
{
    public class MessageDetail
    {

        public string UserName { get; set; }

        public string Message { get; set; }
    
    }
}