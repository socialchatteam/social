﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Social.Domain.Entities;

namespace Social.Data.Abstract
{
    public interface IAuthRepository
    {
        Task<IdentityResult> RegisterUser(UserAuthModel userModel);
        Task<IdentityUser> FindUser(string userName, string password);
        void Dispose();
    }
}
