﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Social.API.Helpers
{
    public enum ResourceUriType
    {
        PreviousPage,
        NextPage
    }
}