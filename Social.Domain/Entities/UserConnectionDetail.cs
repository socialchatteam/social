﻿namespace Social.Domain.Entities
{
    public class UserConnectionDetail
    {
        public string ConnectionId { get; set; }
        public string UserName { get; set; }
    }
}