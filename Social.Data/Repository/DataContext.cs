﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Social.Domain.Entities;

namespace Social.Data.Repository
{
    public class DataContext : DbContext
    {
        public DataContext()
            : base("DataContext")
        {
            
        }

        public DbSet<UserProfile> UserProfiles { get; set; }

        public DbSet<FriendDependence> FriendDependencies { get; set; }

        public int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}
